const express = require("express");


// App server
const app = express();

const port = 3000;


// Middlewares - software that provides common 
// services and capabilities to applications outside
// of what's offered by the operating systemc
app.use(express.json());

// allows your app to read data from any other forms
app.use(express.urlencoded({extended: true}));


// [SECTION] Routes
//Get Method
app.get("/greet", (request, response) => {
	response.send("Hello from the other side /greet endpoint")
});

// Post method

app.post("/hello", (request, response)=>{
	response.send(`Hello MaderFader, ${request.body.firstName} 
		${request.body.lastName}`)
});


// Simple registration form
let users = [];

app.post("/signup", (request, response) =>{
	if (request.body.username !== '' && 
		request.body.password !=='' ){

		users.push(request.body);
		response.send(`User ${request.body.username}
		 successfully registered`)

	}else {
		response.send("Please input both username and firstname")
	}
});

// SImple change password transaction

app.patch("/change-password", (request,response)=>{
	let message;

	for(let i = 0; i< users.length; i++){
		if(request.body.username == users[i].username){
			users[i].password = request.body.password;

			message = `User ${request.body.username}'s 
			password has been updated`;
			break;
		}else{
			message ="user does not exist."
		}
	}

	response.send(message);

});




app.listen(port, () => console.log(`Server running at
	at ${port}`));






/////////////////////////////////////////////////////////////////////////////////


// [SECTION] Activity 34
// 1.

app.get("/home", (request, response) => {
	response.send("Welcome to the homepage!")
});



////////////////////////

// 2.


app.get("/users", (request, response)=>{
	response.send( `[  
						{
							"username": "${request.body.username}"
							"password": "${request.body.password}"
						}
					]`)
});

/*
let user = [];

app.get("/users", (request, response) =>{
	if (request.body.username !== '' && 
		request.body.password !=='' ){

		user.push(request.body);
		response.send(`User ${request.body.username}
		 successfully registered`)

	}else {
		response.send("Please input both username and password")
	}
});
*/
/*
fetch("https://jsonplaceholder.typicode.com/hello/1",{
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	},
	
})

.then((response)=> response.json())
.then((json)=> console.log(`The item "${json.title}" on the list has status of 
	"${json.completed}" script`));


*/


////////////////////////////

// 3.
app.delete("/delete-user", (request, response)=>{
	response.send(` User ${request.body.username} has been deleted`)
});
